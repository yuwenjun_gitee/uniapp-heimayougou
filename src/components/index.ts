import Search from "./Search/index.vue";
const allGolbalComponents: any = { Search };

export default {
  install(app: any) {
    console.log("install",app)
    Object.keys(allGolbalComponents).forEach((key) => {
      app.component(key, allGolbalComponents[key]);
    });
  },
};
