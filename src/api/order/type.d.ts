export interface CreateOrderParams {
  order_price: number;
  consignee_addr: string;
  goods: Goods[];
}

export interface Goods {
  goods_id: number;
  goods_number: number;
  goods_price: number;

  goods_name?: string;
  goods_small_logo?: string;
}

export interface OrderGoods extends Goods {
  goods_total_price: number;
  id: number;
  order_id: number;
}

export interface OrderResult {
  consignee_addr: string;
  create_time: number;
  goods: OrderGoods[];
  is_send: string;
  order_detail: string;
  order_fapiao_company: string;
  order_fapiao_content: string;
  order_fapiao_title: string;
  order_id: number;
  order_number: string;
  order_pay: string;
  order_price: number;
  pay_status: string;
  trade_no: string;
  update_time: number;
  user_id: number;
  total_count?: number;
  total_price?: string;
}

export interface FetchPaymentParams {
  order_number: string;
}

export interface PaymentResult extends FetchPaymentParams {
  pay: Payment;
}

export interface Payment {
  nonceStr: string;
  package: string;
  paySign: string;
  signType: string;
  timeStamp: string;
}

export interface OrderListResult {
  count: number;
  orders: OrderResult[];
}
