import { $http } from "@/utils/http";
import type { CreateOrderParams, FetchPaymentParams, OrderListResult, OrderResult, PaymentResult } from "./type";

enum API {
  CREATEORDER_URL = "/api/public/v1/my/orders/create",
  UNIFIEDORDER_URL = '/api/public/v1/my/orders/req_unifiedorder',
  CHECKORDERSTATUS_URL = '/api/public/v1/my/orders/chkOrder',
  ORDERLIST_URL = '/api/public/v1/my/orders/all'
}

export const postCreateOrderAPI = (data: CreateOrderParams) =>
  $http<OrderResult>({
    url: API.CREATEORDER_URL,
    method: "POST",
    data,
  });

  export const postFetchPaymentAPI = (data: FetchPaymentParams) =>
  $http<PaymentResult>({
    url: API.UNIFIEDORDER_URL,
    method: "POST",
    data,
  });

  export const postCheckOrderStatusAPI = (data: FetchPaymentParams) =>
  $http<string>({
    url: API.CHECKORDERSTATUS_URL,
    method: "POST",
    data,
  });

  export const getOrderListAPI = () =>
  $http<OrderListResult>({
    url: API.ORDERLIST_URL,
    method: "GET"
  });