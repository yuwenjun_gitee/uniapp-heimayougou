export interface CategoriesResult {
    cat_deleted: boolean;
    cat_icon: string;
    cat_id: number;
    cat_level: number;
    cat_name: string;
    cat_pid: number;
    children: CategoriesResult[]
}

