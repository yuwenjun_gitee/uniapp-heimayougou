import { $http } from "@/utils/http";
import type { CategoriesResult } from "./type";

enum API {
    CATEGORIES_URL = '/api/public/v1/categories'
}

export const getCategoriesAPI = () => $http<CategoriesResult[]>({
    url: API.CATEGORIES_URL,
    method: 'GET'
})