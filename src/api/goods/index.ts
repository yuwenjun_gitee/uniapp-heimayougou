import { $http } from "@/utils/http";
import type { SearchItems, SearchResult, SearchParams, GoodsDetailParams, GoodsDetailResult } from "./type";

enum API {
    SEARCH_URL = '/api/public/v1/goods/qsearch',
    SEARCHList_URL = '/api/public/v1/goods/search',
    GOODSDEATIL_URL = '/api/public/v1/goods/detail'
}

export const getSearchListAPI = (data: SearchParams) => $http<SearchResult>({
    url: API.SEARCHList_URL,
    method: 'GET',
    data
})
export const getSearchAPI = (data: SearchParams) => $http<SearchItems[]>({
    url: API.SEARCH_URL,
    method: 'GET',
    data
})

export const getGoodsDetailAPI = (data: GoodsDetailParams) => $http<GoodsDetailResult>({
    url: API.GOODSDEATIL_URL,
    method: 'GET',
    data
})