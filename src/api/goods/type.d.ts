export interface SearchParams {
  query?: string;
  cid?: string;
  pagenum?: number;
  pagesize?: number;
}

export interface SearchResult {
  goods: SearchListItem[];
  pagenum: number;
  total: number;
}
export interface SearchListItem {
  add_time: number;
  cat_id: number;
  cat_one_id: number;
  cat_three_id: number;
  cat_two_id: number;
  goods_big_logo: string;
  goods_id: number;
  goods_name: string;
  goods_number: number;
  goods_price: number;
  goods_small_logo: string;
  goods_weight: number;
  hot_mumber: number;
  is_promote: boolean;
  upd_time: number;
}

export interface SearchItems {
  goods_id: number;
  goods_name: string;
}

export interface GoodsDetailParams {
  goods_id: number;
}

export interface GoodsDetailResult extends SearchListItem {
  delete_time: string;
  goods_cat: string;
  goods_introduce: string;
  goods_state: number;
  is_del: string;
  attrs: AttrsItem[];
  pics: PicsItem[];
}

export interface AttrsItem {
  add_price: number;
  attr_id: number;
  attr_name: string;
  attr_sel: string;
  attr_vals: string;
  attr_value: string;
  attr_write: string;
  goods_id: number;
}

export interface PicsItem {
  goods_id: number;
  pics_big: string;
  pics_big_url: string;
  pics_id: number;
  pics_mid: string;
  pics_mid_url: string;
  pics_sma: string;
  pics_sma_url: string;
}

export interface CartItem {
  goods_id: number;
  goods_name: string;
  goods_price: number;
  goods_count?: number;
  goods_small_logo: string;
  goods_state?: boolean;
}
