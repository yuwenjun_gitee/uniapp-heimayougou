import { $http } from "@/utils/http";
import type { BannerItem, CatItem, CateNavResultItem } from "./type";

enum API {
  SWIPERBANNER_URL = "/api/public/v1/home/swiperdata",
  CATITEMS_URL = '/api/public/v1/home/catitems',
  CATENAV_URL = '/api/public/v1/home/floordata',
}

export const getSwiperBannerAPI = () =>
  $http<BannerItem[]>({
    url: API.SWIPERBANNER_URL,
    method: "GET",
  });

  export const getCatItemsAPI = () =>
  $http<CatItem[]>({
    url: API.CATITEMS_URL,
    method: "GET",
  });

  export const getCateNavAPI = () =>
  $http<CateNavResultItem[]>({
    url: API.CATENAV_URL,
    method: "GET",
  });