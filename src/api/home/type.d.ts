export interface CommonItem {
  image_src: string;
  open_type: string;
  navigator_url: string;
}
export type BannerItem = CommonItem & {
  goods_id: number;
};

export interface CatItem extends CommonItem {
  name: string;
}

export interface CateNavResultItem {
  floor_title: FloorTitle;
  product_list: ProductItem[];
}

export interface FloorTitle {
  name: string;
  image_src: string;
}

export interface ProductItem extends CommonItem {
  image_width: string;
  name: string;
}
