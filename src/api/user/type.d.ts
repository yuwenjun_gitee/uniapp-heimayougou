export interface LoginParams {
  encryptedData: string;
  rawData: string;
  iv: string;
  signature: string;
  code: string;
}

export interface LoginResult {
  token: string;
  user_id: number;
  user_sex: string;
  user_xueli: string;
  user_hobby: string;
  user_introduce: string;
}

export interface redirectInfoParams {
  openType: string;
  from: string;
}

export interface UserInfo {
  avatarUrl: string;
  city: string;
  country: string;
  gender: number;
  language: string;
  nickName: string;
  province: string;
}

export interface AddressInfo {
  cityName: string;
  countyName: string;
  detailInfo: string;
  errMsg: string;
  nationalCode: string;
  postalCode: string;
  provinceName: string;
  telNumber: string;
  userName: string;
}
