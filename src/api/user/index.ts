import { $http } from "@/utils/http";
import type { LoginParams, LoginResult } from "./type";

enum API {
  WXLOGIN_URL = "/api/public/v1/users/wxlogin",
}

export const postLoginAPI = (data: LoginParams) => $http<LoginResult>({
    url: API.WXLOGIN_URL,
    method: 'POST',
    data
})