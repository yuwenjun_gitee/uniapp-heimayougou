import { postLoginAPI } from "@/api/user/index";
import type {
  AddressInfo,
  LoginParams,
  UserInfo,
  redirectInfoParams,
} from "@/api/user/type";

export const useUserStore = defineStore(
  "user",
  () => {
    const userInfo = ref<UserInfo>({
      avatarUrl: "",
      city: "",
      country: "",
      gender: 0,
      language: "",
      nickName: "",
      province: "",
    });
    const token = ref("");
    const redirectInfo = ref<redirectInfoParams>({
      from: "",
      openType: "",
    });
    const addressInfo = ref<AddressInfo>();

    const setUserInfo = (val: UserInfo) => {
      userInfo.value = val;
    };

    const login = async (data: LoginParams) => {
      const res = await postLoginAPI(data);
      token.value = res.message.token;
    };

    const logout = () => {
      token.value = "";
      Object.assign(userInfo.value, {
        avatarUrl: "",
        city: "",
        country: "",
        gender: 0,
        language: "",
        nickName: "",
        province: "",
      });
      Object.assign(redirectInfo.value, {
        from: "",
        openType: "",
      });
      addressInfo.value = undefined;
    };

    const setMockToken = (val: string) => {
      token.value = val;
    };

    const updateRedirectInfo = (obj: redirectInfoParams) => {
      redirectInfo.value = obj;
    };

    const updateAddress = (obj: AddressInfo) => {
      addressInfo.value = obj;
    };

    const addr = computed(() => {
      if (!addressInfo.value) return "";
      const { provinceName, cityName, countyName, detailInfo } =
        addressInfo.value;
      return `${provinceName}  ${cityName} ${countyName} ${detailInfo}`;
    });

    return {
      userInfo,
      token,
      redirectInfo,
      addressInfo,
      addr,
      login,
      logout,
      setUserInfo,
      setMockToken,
      updateRedirectInfo,
      updateAddress,
    };
  },
  {
    persist: {
      storage: {
        getItem(key) {
          return uni.getStorageSync(key);
        },
        setItem(key, value) {
          uni.setStorageSync(key, value);
        },
      },
    },
  }
);
