import type { CartItem } from "@/api/goods/type";

export const useCartStore = defineStore(
  "cart",
  () => {
    const cart = ref<CartItem[]>(
      JSON.parse(uni.getStorageSync("cart") || "[]")
    );

    const addCart = (item: CartItem) => {
      const hasItem = cart.value.find(
        (it: CartItem) => it.goods_id == item.goods_id
      );
      if (hasItem) {
        hasItem.goods_count!++;
      } else {
        cart.value.unshift(item);
      }
    };

    const updateGoodsStatus = (id: number, status: boolean) => {
      const index = cart.value.findIndex(
        (item: CartItem) => item.goods_id == id
      );
      if (index > -1) {
        cart.value[index].goods_state = status;
      }
    };

    const updateGoodsAllStatus = (status: boolean) => {
      cart.value.forEach((item: CartItem) => (item.goods_state = status));
    };

    const removeCartItem = (id: number) => {
      const index = cart.value.findIndex(
        (item: CartItem) => item.goods_id == id
      );
      if (index > -1) {
        cart.value.splice(index, 1);
      }
    };

    const total = computed(() => {
      return cart.value.reduce((prev: number, next: CartItem) => {
        if (next.goods_state) {
          prev += next.goods_price * next.goods_count!;
        }

        return prev;
      }, 0);
    });

    const checkCount = computed(() => {
      return cart.value.filter((item: CartItem) => item.goods_state).length;
    });

    return {
      cart,
      total,
      checkCount,
      addCart,
      updateGoodsStatus,
      updateGoodsAllStatus,
      removeCartItem,
    };
  },
  {
    // 重要：模块必须开启持久化，才会持久化！！！
    // 注意：默认持久化，只在H5端有效，因为默认使用的是localStorage，小程序端不能使用这个API，所以需要重写存取方法，转调给UniApp的持久化方法
    // persist: true,
    persist: {
      // 默认值，就是使用localStorage
      // storage: localStorage
      // 重写存取方法，转调给UniApp
      storage: {
        getItem(key) {
          return uni.getStorageSync(key);
        },
        setItem(key, val) {
          uni.setStorageSync(key, val);
        },
      },
    },
  }
);
