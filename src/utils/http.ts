import type { Data } from "@/types/global";
import { hideLoading, showLoading } from "./loading";
import { BASE_URL } from "./config";
import { useUserStore } from "@/store/modules/user";

const httpInterceptor = {
  invoke(options: UniApp.RequestOptions) {
    if (!options.url.startsWith("http")) {
      options.url = BASE_URL + options.url;
    }
    options.timeout = 10000;
    //添加请求头
    const userStore = useUserStore();
    const { token } = storeToRefs(userStore);
    if (token.value) {
      options.header = {
        Authorization: token.value,
      };
    }

    showLoading();
  },
};

uni.addInterceptor("request", httpInterceptor);
uni.addInterceptor("uploadFile", httpInterceptor);

export const $http = <T>(options: UniApp.RequestOptions) => {
  return new Promise<Data<T>>((resolve, reject) => {
    uni.request({
      ...options,
      success: (res) => {
        const result = res.data as Data<T>;
        if (result.meta.status == 200) {
          resolve(result);
        } else if (result.meta.status == 401) {
          //跳转到登录页
          console.log("跳转到登录页");
          reject(res);
        } else {
          uni.showToast({
            icon: "none",
            title: (result.meta.msg) || "请求出错",
          });
          reject(res);
        }
      },
      fail: (err) => {
        uni.showToast({
          icon: "error",
          title: "网络错误，换个网络试试",
        });
        reject(err);
      },
      complete: () => {
        hideLoading();
      },
    });
  });
};
