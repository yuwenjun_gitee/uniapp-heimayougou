let reqNum = 0;
export const showLoading = () => {
  if (reqNum === 0) {
    uni.showLoading({
      title: "加载中...",
    });
  } else {
    reqNum++;
  }
};

export const hideLoading = () => {
  if (reqNum === 0) {
    uni.hideLoading();
  } else if (reqNum <= 0) {
    reqNum = 0;
  } else {
    reqNum--;
  }
};
