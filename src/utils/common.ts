const debounce = (fn: Function, delay: number) => {
    let timer = 0
    return () => {
        clearTimeout(timer)
        timer = setTimeout(() => {
            fn()
        }, delay)
    }
}

export {
    debounce
}