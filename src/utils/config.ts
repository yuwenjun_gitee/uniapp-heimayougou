const BASE_URL = "https://api-hmugo-web.itheima.net";
const OPTIONS = [
  {
    icon: "shop",
    text: "店铺",
    info: 0,
    infoBackgroundColor: "#007aff",
    infoColor: "red",
  },
  {
    icon: "cart",
    text: "购物车",
    info: 0,
  },
];
const BUTTONGROUP = [
  {
    text: "加入购物车",
    backgroundColor: "#ff0000",
    color: "#fff",
  },
  {
    text: "立即购买",
    backgroundColor: "#ffa200",
    color: "#fff",
  },
];

const NAVLIST = [
  {
    num: 8,
    title: "收藏的店铺",
  },
  {
    num: 20,
    title: "收藏的商品",
  },
  {
    num: 52,
    title: "关注的商品",
  },
  {
    num: 66,
    title: "足迹",
  },
];

const ORDERNAVLIST = [
  {
    icon: '/static/my-icons/icon1.png',
    title: '待付款'
  },
  {
    icon: '/static/my-icons/icon2.png',
    title: '待收货'
  },
  {
    icon: '/static/my-icons/icon3.png',
    title: '退款/退货'
  },
  {
    icon: '/static/my-icons/icon4.png',
    title: '全部订单'
  },
]

export { BASE_URL, OPTIONS, BUTTONGROUP,NAVLIST,ORDERNAVLIST };
