export interface Data<T> {
  meta: Meta;
  message: T;
}
interface Meta {
  msg: string;
  status: number;
}
